<script src="js/jquery.min.js"></script>
<script src="js/app.js"></script>
<script src="js/adminlte.js"></script>
<script>
			$(".pilih").change(function(){
				var jumlah_pilih = document.getElementById("jumlah_pilih");
				var m = document.querySelectorAll('input[name="pilih[]"]:checked').length;
				jumlah_pilih.innerHTML =m;
			});

			function checkAll(box) 
				{
				let checkboxes = document.getElementsByClassName('pilih');

				if (box.checked) { // jika checkbox teratar dipilih maka semua tag input juga dipilih
					for (let i = 0; i < checkboxes.length; i++) {
					if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = true;
					}
					}
				} else { // jika checkbox teratas tidak dipilih maka semua tag input juga tidak dipilih
					for (let i = 0; i < checkboxes.length; i++) {
					if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = false;
					}
					}
				}
				var jumlah_pilih = document.getElementById("jumlah_pilih");
				var m = document.querySelectorAll('input[name="pilih[]"]:checked').length;
				jumlah_pilih.innerHTML =m;
				}
				
		</script>
		<script>
		</script>
		<script type="text/javascript">
			
				$(document).ready(function($){
				
				   $('body').on('click', '#aktifkan', function () {
					   var id = $(this).data('id');
					   var pilih=[]; 
					   $('.pilih').each(function(){
							if($(this).is(":checked"))
							{
								pilih.push($(this).val());
							}
						})
						
					   $.ajax({
						   type:"POST",
						   url: "{{ url('/aktifkan') }}",
						   data: { 
							   pilih: pilih,
							   _token: '{{csrf_token()}}'
							    },
						   dataType: 'json',
						   success: function(res){
							window.location.reload();
							console.log(pilih);
						  }
					   });
				   });

				   $('body').on('click', '#komplitkan', function () {
					   var id = $(this).data('id');
					   var pilih=[]; 
					   $('.pilih').each(function(){
							if($(this).is(":checked"))
							{
								pilih.push($(this).val());
							}
						})
						// pilih = pilih.toString();
						
					   $.ajax({
						   type:"POST",
						   url: "{{ url('/komplitkan') }}",
						   data: { 
							   pilih: pilih,
							   _token: '{{csrf_token()}}'
							    },
						   dataType: 'json',
						   success: function(res){
							window.location.reload();
						  }
					   });
				   });
			
				   $('.hapus').on('click', function () {
						var id = $(this).data('id');
					   var teks = $(this).data('teks');
					   
					  if (confirm("Hapus note '"+teks+"' ?") == true) {
						
					   $.ajax({
						   type:"POST",
						   url: "{{ url('/hapus') }}",
						   data: { 
							   id: id,
							   _token: '{{csrf_token()}}'
							   },
						   dataType: 'json',
						   success: function(res){
							 window.location.reload();
						  }
					   });
					  }
				   });

				   $('body').on('click', '#hapus_completed', function () {
					   
					  if (confirm("Hapus semua note berstatus 'completed' ?") == true) {
						
					   $.ajax({
						   type:"GET",
						   url: "{{ url('/hapus_completed') }}",
						   data: { 
							   _token: '{{csrf_token()}}'
							   },
						   dataType: 'json',
						   success: function(res){
							 window.location.reload();
						  }
					   });
					  }
				   });
				
				let input = document.getElementById("teks");
				input.addEventListener('keyup', (e) => {
					if (e.keyCode === 13) 
					{
						var teks = $("#teks").val();
						
					   $.ajax({
						   type:"POST",
						   url: "{{ url('/tambah') }}",
						   data: {
							 teks:teks,
							 _token: '{{csrf_token()}}'
						   },
						   dataType: 'json',
						   success: function(res){
							window.location.reload();
						  }
					   });

				}
			});
			   });
</script>





