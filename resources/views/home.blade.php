
@extends('main')
@section('judul')
        <title>Super2Do</title>
@endsection
@section('isi')

	<body>
		<section class="todoapp">
			<header class="header">
				<h1>Super2Do</h1>
			<form action="javascript:void(0)" id="addEditBookForm" name="addEditBookForm" method="POST">
				@csrf
				<input class="new-todo" type="text" id="teks" name="teks" placeholder="What needs to be done?" autofocus required>
			</form>
			</header>
			<!-- This section should be hidden by default and shown when there are todos -->
			<section class="main">
				<input id="toggle-all" class="toggle-all" type="checkbox">
				<label for="toggle-all">Mark all as complete</label>
				<ul class="todo-list">
					<!-- These are here just to show the structure of the list items -->
					<!-- List items should get the class `editing` when editing and `completed` when marked as completed -->
					@foreach ($note as $note)
					@if($note->status=='active')
						<li class="active">
					@elseif($note->status=='completed')
						<li class="completed">
					@endif
						<div class="view">
							<input class="toggle pilih" name="pilih[]" value="{{ $note->id }}" type="checkbox">
							<label> {{ $note->teks }}</label>
							<form action="javascript:void(0)" method="POST">
							<button data-id="{{ $note->id }}" data-teks="{{ $note->teks }}" class="destroy hapus"></button>
							</form>
						</div>
						<input class="edit" value="Create a Todo template">
					</li>
					@endforeach
				</ul>
			</section>
			
			<!-- This footer should be hidden by default and shown when there are todos -->
			<footer class="footer">
				<!-- This should be `0 items left` by default -->
				<span class="todo-count"><strong id="jumlah_pilih">0</strong> item left</span>
				<!-- Remove this if you don't implement routing -->
				<ul class="filters">
					<li>
						<a class="selected" href="#">
							<label for="pilihan" id="pilihsemua">
							All
						</label>
						</a>
					
						<input id="pilihan" onchange="checkAll(this)" style="display: none" type="checkbox">
					</li>
					<li>
						<a id="aktifkan" href="#">Active&nbsp;<span class="badge badge-light">({{ $jumlah_aktif }})</span></a>
					</li>
					<li>
						<a href="#" id="komplitkan">Completed&nbsp;<span class="badge badge-light">({{ $jumlah_completed }})</span></a>
					</li>
				</ul>
				<!-- Hidden if no completed items are left ↓ -->
				<button id="hapus_completed" href="#" class="clear-completed">Clear completed</button>
			</footer>
		</section>
		Selamat datang di note anda, {{ auth()->user()->name }}
		<form id="logout-form" action="{{ route('logout') }}" method="POST">
			@csrf
			<input type="hidden" name="" id="input">
			Klik <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit();">disini</a> untuk keluar
		  </form>
	
        @endsection
		@section('foot_dinamis')
		
		@endsection