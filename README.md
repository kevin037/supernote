How to deploy :
1. buat database baru bernama "supernote"
2. lakukan migrasi pada terminal laravel untuk memperbarui tabel pada database yang telah dibuat
3. ketikan "php artisan serve" pada terminal untuk memulai project pada server
4. Aplikasi siap digunakan 

Cara penggunaan :
1. Masukkan email dan password untuk login (jika belum terdaftar, klik "Daftar sekarang")
2. User dapat menambah notes, mengubah status, serta menghapus sesuai dengan kebutuhan


=================================================================================================

Penggunaan Endpoint API
1. Mendapatkan data notes pada user tertentu, teridentifikasi dengan "id"
Route::get('note{id}',[ApiController::class,'note']);

2. Mendapatkan data jumlah notes berstatus aktif pada user tertentu, teridentifikasi dengan "id"
Route::get('jumlah_aktif{id}',[ApiController::class,'jumlah_aktif']);

3. Mendapatkan data jumlah notes berstatus completes pada user tertentu, teridentifikasi dengan "id"
Route::get('jumlah_completed{id}',[ApiController::class,'jumlah_completed']);

4. Menambah data notes pada user tertentu, teridentifikasi dengan "id"
Route::post('tambah{id}',[ApiController::class,'tambah']);

5. Mengubah data notes berstatus aktif menjadi completed pada user tertentu
Route::put('aktifkan',[ApiController::class,'aktifkan']);

6. Mengubah data notes berstatus completed menjadi aktif pada user tertentu
Route::put('komplitkan',[ApiController::class,'komplitkan']);

7. Menghapus data notes pada user tertentu
Route::delete('hapus',[ApiController::class,'hapus']);

8. Menghapus seluruh data notes berstatus completed pada user tertentu, teridentifikasi dengan "id"
Route::delete('hapus_completed{id}',[ApiController::class,'hapus_completed']);

