<script>
  $(document).ready(function(){
  
  $('select[name="provinsi_tujuan"]').on('change', function(){
  
  let provinceId = $(this).val();
  if(provinceId){
      
      $.ajax({
          url: '/provinsi/'+provinceId+'/kota',
          type:"GET",
          dataType:"json",
          success:function(data){    
            $('select[name="kota_tujuan"]').empty();
            $.each(data, function(i, data){
              $('select[name="kota_tujuan"]').append('<option value="'+data.city_id+'">' +data.title+'</option>');
            });
          },
      });
  }else{
    $('select[name="kota_tujuan"]').empty();
  }
  });
  
  });
  
  
  
  $('#paket').on('change', function(){
  const price = parseInt($('#paket option:selected').val());
  const berat = parseInt($('[name=berat]').val());
  const produk = parseInt($('#biaya_produk').val());
  const biaya_produk_input = parseInt($('[name=biaya-produk-input]').val());
  const jumlah_pembelian = parseInt($('[name=jumlah_pembelian').val());
  
  //  const harga_produk_div = parseInt($('[name=harga-produk-input]').val());
  const formatRupiah = (money) => {
  return new Intl.NumberFormat('id-ID',
    {style:'currency',currency:'IDR', minimumFractionDigits: 0 }
  ).format(money);
  }
  
  if (berat <= '1000') {
  const n = 1;
  const total = price * n;
  var total2 =parseInt(total)  + produk;
  
  $('#input_ongkir').val(total);
  
  var hasil_ongkir = document.getElementById("hasil_ongkir");
  var biaya_produk = document.getElementById("biaya-produk-div");
  var hasil_total = document.getElementById("hasil_total");
  var diskon = document.getElementById("diskon");
  
  hasil_ongkir.innerHTML = formatRupiah(total); 
  biaya_produk.innerHTML = formatRupiah(biaya_produk_input); 
  
  if(jumlah_pembelian == '2'){
    var total2_diskon = parseInt(total2) - 15000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(15000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(15000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else if(jumlah_pembelian >= '3'){
    var total2_diskon = parseInt(total2) - 20000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(20000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(20000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else{
    $('#biaya_final').val(total2);
    $("#biaya_final").attr('data-id',total2);
    
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(0)+"</div>"; 
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2)+"</p>";
  }
  
  }
  
  
  else if(berat > '1000' && berat <= '2000'){
  const n = 2;
  const total = price * n;
  var total2 =parseInt(total)  + produk;
  
  $('#input_ongkir').val(total);
  
  var hasil_ongkir = document.getElementById("hasil_ongkir");
  var biaya_produk = document.getElementById("biaya-produk-div");
  var hasil_total = document.getElementById("hasil_total");
  var diskon = document.getElementById("diskon");
  
  hasil_ongkir.innerHTML = formatRupiah(total); 
  biaya_produk.innerHTML = formatRupiah(biaya_produk_input); 
  
  if(jumlah_pembelian == '2'){
    var total2_diskon = parseInt(total2) - 15000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(15000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(15000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else if(jumlah_pembelian >= '3'){
    var total2_diskon = parseInt(total2) - 20000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(20000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(20000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else{
    $('#biaya_final').val(total2);
    $("#biaya_final").attr('data-id',total2);
    
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(0)+"</div>"; 
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2)+"</p>";
  }
  }
  
  else if(berat > '2000' && berat <= '3000'){
  var n = 3;
  var total = price * n;
  var total2 =parseInt(total)  + produk;
  
  $('#input_ongkir').val(total);
  
  var hasil_ongkir = document.getElementById("hasil_ongkir");
  var biaya_produk = document.getElementById("biaya-produk-div");
  var hasil_total = document.getElementById("hasil_total");
  var diskon = document.getElementById("diskon");
  
  hasil_ongkir.innerHTML = formatRupiah(total); 
  biaya_produk.innerHTML = formatRupiah(biaya_produk_input); 
  
  if(jumlah_pembelian == '2'){
   var total2_diskon = parseInt(total2) - 15000;
   $('#biaya_final').val(total2_diskon);
   $('#input_diskon').val(15000);
   $("#biaya_final").attr('data-id',total2_diskon);
  
   diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(15000)+"</div>";
   hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else if(jumlah_pembelian >= '3'){
    var total2_diskon = parseInt(total2) - 20000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(20000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(20000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else{
   $('#biaya_final').val(total2);
   $("#biaya_final").attr('data-id',total2);
   
   diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(0)+"</div>"; 
   hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2)+"</p>";
  }
  }
  else if(berat > '3000' && berat <= '4000'){
  const n = 4;
  const total = price * n;
  var total2 =parseInt(total)  + produk;
  
  $('#input_ongkir').val(total);
  
  var hasil_ongkir = document.getElementById("hasil_ongkir");
  var biaya_produk = document.getElementById("biaya-produk-div");
  var hasil_total = document.getElementById("hasil_total");
  var diskon = document.getElementById("diskon");
  
  hasil_ongkir.innerHTML = formatRupiah(total); 
  biaya_produk.innerHTML = formatRupiah(biaya_produk_input); 
  
  if(jumlah_pembelian == '2'){
   var total2_diskon = parseInt(total2) - 15000;
   $('#biaya_final').val(total2_diskon);
   $('#input_diskon').val(15000);
   $("#biaya_final").attr('data-id',total2_diskon);
  
   diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(15000)+"</div>";
   hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else if(jumlah_pembelian >= '3'){
    var total2_diskon = parseInt(total2) - 20000;
    $('#biaya_final').val(total2_diskon);
    $('#input_diskon').val(20000);
    $("#biaya_final").attr('data-id',total2_diskon);
  
    diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(20000)+"</div>";
    hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2_diskon)+"</p>";
  }
  else{
   $('#biaya_final').val(total2);
   $("#biaya_final").attr('data-id',total2);
   
   diskon.innerHTML ="<div class='col-9'>Diskon&nbsp;<i class='fa fa-info-circle' type='button' data-toggle='modal' data-target='#modal_promo_checkout' aria-hidden='true'></i></div><div class='col-3'>-"+formatRupiah(0)+"</div>"; 
   hasil_total.innerHTML = "<p class='fw-bold'>"+formatRupiah(total2)+"</p>";
  }
  }
  });
  </script>