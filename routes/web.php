<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


Route::get('/',[HomeController::class,'index']);
Route::get('/home',[HomeController::class,'index']);
Route::post('/tambah',[HomeController::class,'tambah']);
Route::post('/aktifkan',[HomeController::class,'aktifkan']);
Route::post('/komplitkan',[HomeController::class,'komplitkan']);
Route::post('/hapus',[HomeController::class,'hapus']);
Route::get('/hapus_completed',[HomeController::class,'hapus_completed']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
