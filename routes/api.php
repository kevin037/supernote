<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('note{id}',[ApiController::class,'note']);
Route::get('jumlah_aktif{id}',[ApiController::class,'jumlah_aktif']);
Route::get('jumlah_completed{id}',[ApiController::class,'jumlah_completed']);
Route::post('tambah{id}',[ApiController::class,'tambah']);
Route::put('aktifkan',[ApiController::class,'aktifkan']);
Route::put('komplitkan',[ApiController::class,'komplitkan']);
Route::delete('hapus',[ApiController::class,'hapus']);
Route::delete('hapus_completed{id}',[ApiController::class,'hapus_completed']);