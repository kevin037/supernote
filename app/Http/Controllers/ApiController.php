<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;

class ApiController extends Controller
{

    public function note($id)
    {
        $note = Note::where('user_id',$id)->orderBy('id', 'desc')->get();
        return response()->json($note);
    }

    public function jumlah_aktif($id){
        $jumlah_aktif = Note::where('user_id',$id)->where('status','=','active')->count();
        return response()->json($jumlah_aktif);
    }

    public function jumlah_completed($id){
        $jumlah_completed = Note::where('user_id',$id)->where('status','=','completed')->count();
        return response()->json($jumlah_completed);
    }

    public function tambah(Request $request, $id)
    {
        $note = new Note;
        $note->teks = $request->teks;
        $note->status = "active";
        $note->user_id = $id;
        $note->save(); 
        return response()->json($note);
    }

    public function aktifkan(Request $request)
    {   
        $id_note['pilih'] = $request->input('pilih');
        foreach ($id_note as $id) {
            $note = Note::whereIn('id',$id)->where('status','=','completed')
        ->update([
            'status' => 'active',
        ]);
        }
        return response()->json($note);
    }

    public function komplitkan(Request $request)
    {   
        $id_note['pilih'] = $request->input('pilih');
        foreach ($id_note as $id) {
            $note = Note::whereIn('id',$id)->where('status','=','active')
        ->update([
            'status' => 'completed',
        ]);
        } 
        return response()->json($note);
    }

    public function hapus(Request $request)
    {
        $note = Note::where('id',$request->id)->delete();
   
        return response()->json($note);
    }

    public function hapus_completed($id)
    {
        $note = Note::where('status','=','completed')->where('user_id', $id)->delete();
   
        return response()->json($note);
    }
}
