<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = auth()->user()->id;
        $note = Note::where('user_id',$id)->orderBy('id', 'desc')->get();
        $jumlah_aktif = Note::where('user_id',$id)->where('status','=','active')->count();
        $jumlah_completed = Note::where('user_id',$id)->where('status','=','completed')->count();

        $status="masuk";
        return view('home',compact('status','note','jumlah_aktif','jumlah_completed'));
    }

    public function tambah(Request $request)
    {
        $note = new Note;
        $note->teks = $request->teks;
        $note->status = "active";
        $note->user_id = auth()->user()->id;
        $note->save(); 
        return response()->json($note);
    }

    public function aktifkan(Request $request)
    {   
        $id_note['pilih'] = $request->input('pilih');
        foreach ($id_note as $id) {
            $note = Note::whereIn('id',$id)->where('status','=','completed')
        ->update([
            'status' => 'active',
        ]);
        }
        return response()->json($note);
    }

    public function komplitkan(Request $request)
    {   
        $id_note['pilih'] = $request->input('pilih');
        foreach ($id_note as $id) {
            $note = Note::whereIn('id',$id)->where('status','=','active')
        ->update([
            'status' => 'completed',
        ]);
        } 
        return response()->json($note);
    }

    public function hapus(Request $request)
    {
        $note = Note::where('id',$request->id)->delete();
   
        return response()->json($note);
    }

    public function hapus_completed()
    {
        $id = auth()->user()->id;
        $note = Note::where('status','=','completed')->where('user_id', $id)->delete();
   
        return response()->json($note);
    }
}
